#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_phoenixin.mk

COMMON_LUNCH_CHOICES := \
    lineage_phoenixin-user \
    lineage_phoenixin-userdebug \
    lineage_phoenixin-eng
